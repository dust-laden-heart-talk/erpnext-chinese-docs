# 数据安全 ：备份与迁移

数据持久化

## Docker 下的备份与还原

### 备份：
```shell
docker run \
    -e "SITES=site1.domain.com:site2.domain.com" \
    -e "WITH_FILES=1" \
    -v <project-name>_sites-vol:/home/frappe/frappe-bench/sites \
    --network <project-name>_default \
    frappe/erpnext-worker:version-12 backup
```
备份文件的路径在 sites-vol:/home/frappe/frappe-bench/sites/<site-name>/private/backups
```shell
docker cp <project-name>_erpnext-worker-default_1:/home/frappe/frappe-bench/sites/<site-name>/private/backups ~/erpnext
```
### 还原

Backup structure for mounted volume or downloaded from s3:
备份文件请先按照下面的格式整理好：
backups
- site1.domain.com
  - 20200420_162000
    - 20200420_162000-site1_domain_com-*
- site2.domain.com
  - 20200420_162000
    - 20200420_162000-site2_domain_com-*

然后把 backups 映射到 /home/frappe/backups

``` shell
docker run \
    -e "MYSQL_ROOT_PASSWORD=<mysql_pwd>" \
    -v <project-name>_sites-vol:/home/frappe/frappe-bench/sites \
    -v ./backups:/home/frappe/backups \
    --network <project-name>_default \
    frappe/frappe-worker:version-12 restore-backup
```

## Docker下的数据迁移（切换宿主机）

数据迁移需要一下条件：
- 环境代码一致：
  - project-name 要一样
  - .env 环境变量内容要一样，尤其是sitename
  - 版本一样，跨版本未做测试。
- 数据文件要一致：
  - 旧的数据文件复制到新的环境中。

> <font color='red'>警告：操作有风险，数据要备份。</font>


### 步骤一：复制代码库
把旧主机的 frappe_docker复制到新主机上。

### 步骤二：复制卷文件
0、停止旧主机的容器。
```shell
# 进入 frappe_docker 文件夹操作
docker-compose --project-name erp stop
```

1、压缩volumes。
```shell
mkdir ~/erpnext # 可选
# 压缩卷文件
sudo tar -zcvf ~/erpnext/volumes_bak.tar.gz\
 /var/lib/docker/volumes/<project-name>_mariadb-vol\
 /var/lib/docker/volumes/<project-name>_redis-cache-vol\
 /var/lib/docker/volumes/<project-name>_redis-queue-vol\
 /var/lib/docker/volumes/<project-name>_redis-socketio-vol\
 /var/lib/docker/volumes/<project-name>_assets-vol\
 /var/lib/docker/volumes/<project-name>_sites-vol\
 /var/lib/docker/volumes/<project-name>_cert-vol
```

2、把 volumes_bak.tar.gz 复制到新的主机上面，并解压。
```shell
# 压缩文件包含了路径 /var/lib/docker/volumes ，因此直接解压到根目录就可以了。
sudo tar -C / -zxvf ~/erpnext/volumes_bak.tar.gz
```
### 步骤三：启动容器
``` shell
docker-compose --project-name erp up -d
```

