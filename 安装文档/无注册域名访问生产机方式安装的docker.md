请先参考docker生产环境安装
![docker-compose项目正常启动](https://images.gitee.com/uploads/images/2020/1119/234203_4383e071_7624863.png "屏幕截图.png")
1. frappe_docker目录下的.env文件
SITE_NAME=myerp.cn
SITES=`myerp.cn`

2. 移除docker-compose.yml中的traefik服务，删除erpnext-nginx服务中traefik相关标签，在erpnext-nginx中增加端口

3. 启动docker-compose 后，(yu是项目名）通过 docker inspect yu_default 查询容器名为yu_erpnext-nginx_1的内部IP
![查容器IP](https://images.gitee.com/uploads/images/2020/1119/233611_ba0a3fbe_7624863.png "屏幕截图.png")
![容器nginx IP](https://images.gitee.com/uploads/images/2020/1119/233659_1f9969a3_7624863.png "屏幕截图.png")

4. 修改宿主机nginx配置文件增加以下反向代理服务器设置，server_name 随便取，关键是X-Frappe-Site-Name 和 Host 后都设为自己当时创建的站点名（目录名），以及这个内部IP

```
server {
                server_name yuzeling;
                listen 8090;
                location / {
                        proxy_set_header Host myerp.cn;
	                proxy_set_header X-Frappe-Site-Name myerp.cn;
                        proxy_set_header X-Real-IP $remote_addr;
                        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                        proxy_pass http://172.20.0.4;
                }
        }
```
![server设置](https://images.gitee.com/uploads/images/2020/1120/222337_44642ee5_7624863.png "屏幕截图.png")

5. 使用宿主IP和nginx里配置的端口访问
![网址](https://images.gitee.com/uploads/images/2020/1119/234011_22cf37a6_7624863.png "屏幕截图.png")