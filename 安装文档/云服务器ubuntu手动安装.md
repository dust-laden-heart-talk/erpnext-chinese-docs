本安装步骤主要参照这两份英文安装说明 
	官网www.github.com/frappe/bench 文档中链接的手动安装说明https://github.com/frappe/frappe/wiki/The-Hitchhiker%27s-Guide-to-Installing-Frappe-on-Linux
	官网论坛里社区用户作的手册https://discuss.erpnext.com/t/guide-manual-install-erpnext-on-ubuntu-17-xx-18-xx/36745

这份阿里云安装说明，与本安装过程高度相似https://developer.aliyun.com/article/723355

前期准备
•	云服务器，操作系统选ubuntu18.04 64位并已初始化, 推荐至少2G内存，40G 硬盘
•	SSH工具：如putty, 或SecureCRT

1.	创建一个有sudo权限的用户帐号(如果云服务器上已创建该帐号，则可忽略该步骤)

用root帐号登录
adduser fisher
usermod -aG sudo fisher
su – fisher
（切换为非root帐号，此帐号将用户安装与管理新安装的ERPNext系统）

2.	升级操作系统

sudo apt -y update
sudo apt -y upgrade
(约2分钟进度97%时输入Y 回车继续,阿里云没提示)

3.	安装git, python3等相关依赖包

sudo apt-get install git
输入Y 回车继续
sudo apt-get install python3-dev python3-setuptools python3-pip
（输入Y 回车继续,阿里云不用输入Y确认)
sudo apt-get install virtualenv
（输入Y 回车继续)
alias python=python3
alias pip=pip3

4.	安装数据库并调整参数

sudo apt-get install mariadb-server
输入Y 回车继续

sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf

[mysqld]
skip-grant-tables
innodb-file-format=barracuda
innodb-file-per-table=1
innodb-large-prefix=1
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci

在[mysqld]下面粘贴上述内容（复制后点右键），再将光标移动到底下，找到这一行，在前面加#号注释掉
#collation-server = utf8mb4_general_ci
ctrl+O, 提示保存修改内容按回车确认，再Ctrl + X 回到命令行

sudo service mysql restart

mysql -u root
以下是在数据库命令窗口执行，分号是代码结束符

use mysql;
UPDATE user SET plugin='mysql_native_password' WHERE User='root';
UPDATE mysql.user set password = PASSWORD('你的密码') where user = 'root' and host = 'localhost';
FLUSH PRIVILEGES;
exit;

sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf

[mysqld]
skip-grant-tables

把这个参数skip-grant-tables删除（不做这一步后续安装erpnext会提示数据库不安全不允许创建数据库）
ctrl+O, 提示保存修改内容按回车确认，再Ctrl + X 回到命令行

sudo service mysql restart

mysql -u root -p -h localhost
根据提示输入密码，用此命令验证是否可以正常登录数据库了

sudo apt-get install libmysqlclient-dev
（输入Y 回车继续)

5.	安装redis缓存数据库，nodejs，yarn

sudo apt-get install redis-server
（输入Y 回车继续)

sudo apt-get install curl
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo npm install -g yarn

6.安装bench

sudo chown -R fisher .config
此步骤只适用阿里云，将用户家目录下的.config文件夹Owner由root变更为当前用户，以避免后面下载和安装应用时提示权限错误，

git clone https://github.com/frappe/bench
此命令是下载整个软件包到本地目录，视网速，等约两三分钟

pip3 install -e ./bench
将下载的目录安装到python库中，约2分钟

上述两个命令可用 pip3 install frappe-bench命令代替，官方推荐的bench安装方式（经测试，会导致最后的sudo bench setup production fisher命令失败，可能是这个已发布版不是最新的，所以暂时不推荐使用这个命令安装bench）
7. 重启系统
sudo reboot

等约一分钟后，重新使用ssh连接并登录系统

bench -–version
验证bench 是否成功安装，

到这一步大概20分钟

8. 使用bench 生成bench实例，安装应用, 创建站点，设置生产环境

bench init --frappe-branch develop frappe-bench
此步骤生成一个独立的bench实例，即独立运行环境（python的虚拟环境），如常见的开发、测试、生产环境。对应独立的文件目录，与站点不同，各bench实例之间的代码也是独立的。分支可以换成version-12或省略--frappe-branch参数以安装目前的稳定版，此步骤视网速，需下载11.91M并安装为Python包/模块,耗时较长(0.5~1小时)

有黄色警告提示WARN: Command not being executed in bench directory和
以下红色出错提示，均可忽略，会自动转为下载自行安装（阿里云没有此提示）
 
如果超过两小时停留在最后的build步骤（如下图），这一步就安装失败了，可以ctrl + c中止，再执行命令 rm -R frappe-bench 删除已生成的目录（即卸载未成功安装产生的内容），可再重复执行bench init命令
 

cd frappe-bench
切换到bench 目录

bench get-app --branch develop erpnext
下载erpnext应用，指定分支（版本），此处的分支参数应该与bench init的一致，需下载ERP 应用（15.14M）并安装为python 包/模块，耗时较长（0.5~1小时）

bench new-site site1.local
创建新站点和后台数据库,每个站点对应一个后台数据库，可以选择性地安装已加载到bench实例的应用，各站点之间是应用的代码是共用的。
根据提示输入数据库root帐号密码（就是上面处理数据库时用到的），和ERP系统管理员帐号密码，首次登录网页时要用，也可以在命令行中直接添加以上所需的密码及数据库名等参数。

bench install-app erpnext

sudo pip3 install -e  ../bench
将bench作为模块安装到bench实例的虚拟环境中，以便下面的命令可正常运行

sudo bench setup production fisher
此命令中的fisher是最开始创建的非root用户名，此命令是将安装的系统设置为生产环境，用nginx作网页反向代理服务器，用户通过网址直接访问（使用标准80端口），使用supervisor管理全部应用进程，过程中会下载安装nginx, supervisor以及最后会编译Js文件，耗时较长，默认安装的系统作为开发环境使用，使用bench start命令启动服务，默认使用8000端口号，用户从网页登录系统时网址后要加:8000端口号
 
最后安装完就长这样了

使用浏览器登录，完成初始化设置后，就看到下面的主界面。
 

其它常用bench 命令

bench restart  重启服务
bench clear-cache  清缓存
bench build    重编译JS文件
bench console  进入交互式python命令界面，可测试，调试
bench mariadb  进入交互式数据库命令界面，可直接操作后台数据，建议只作查询分析，不要直接修改删除数据，否则容易导致整个系统瘫痪
bench new-app  生成一个新的APP,目录
bench update  谨慎使用，这个是系统升级命令
bench backup  备份数据库与文档附件
bench restore 恢复数据库

小贴士：
	使用bench xxx --help xxx代表具体的命令，可直接bench --help查看具体命令及参数
	Mariadb 命令行界面，如果查询数据太长，后面有省略号，按回车会继续显示后续内容，敲字母q取消后面的数据显示，返回命令行，exit命令返回terminal命令行

常见问题
1.	提示timeout超时，
原因：连接了国外的软件仓库，
解决方案：重新执行命令，必要时可考虑换成高带宽，网络好的服务商，如中国电信，或使用pip -i 参数将软件仓库指向国内源
如果是github，可以将github.com/frappe等代码同步到国内的码云代码托管服务器上，在init 或 get-app里指定代码仓库

2.	提示没有权限,如config/git/attributes': Permission denied
原因：目录或文件的所有者没设为了root.
解决方案：chmod 777 或 chown
chown -R fisher .config
chmod -R 777 .config

3.	提示bench命令不是有效命令
原因：当前目前不是bench 目录
解决方案：用cd 命令切换到bench目录，bench目录 /home/fisher/frappe-bench

4.	sudo: bench: command not found
原因：bench本身未被作为模块安装到bench 实例（bench目录代表的python虚拟环境）
解决方案： 回到用户根目录，sudo pip3 install -e frappe-bench
最后的frappe-bench是当时安装
