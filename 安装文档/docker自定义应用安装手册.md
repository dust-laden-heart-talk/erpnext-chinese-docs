## 介绍

本文档参考 https://github.com/castlecraft/custom_frappe_docker 的方式进行构建包含自定义app的镜像，并通过构建的镜像进行生产环境的安装。

- 本文以余则霖整合的 [erpnext_chinese](https://gitee.com/yuzelin/erpnext_chinese) app为例子。
- 本文选择 Version - 13 版本进行安装演示。

感谢所有大神的贡献。

## 依赖

- docker
- docker-compose
- git

以上依赖请自行安装。



## 安装步骤

### 1. 克隆代码到本地

`git clone https://github.com/castlecraft/custom_frappe_docker`



### 2. 切换到安装目录

`cd custom_frappe_docker`

### 3. 修改 `nginx/Dockerfile` 里的自定义app

将需要安装的 `erpnext_chinese` 插入到下面位置，源代码库包含其他app，本文不做安装演示，所以删掉。

```dockerfile
  # Install required apps with following command
  # /install_app <app_name> <repo> <app_branch>
  /install_app erpnext_chinese https://gitee.com/lavenza/erpnext_chinese master && \
  # Cleanup for production
```

### 4. 修改`worker/Dokerfile`里的自定义app

```dockerfile
ARG FRAPPE_BRANCH=develop
FROM frappe/erpnext-worker:${FRAPPE_BRANCH}

RUN install_app erpnext_chinese https://gitee.com/lavenza/erpnext_chinese master
```

### 5. 分别 Build 两个镜像

这里用 version-13 这个版本来 build：

```bash
# build nginx镜像
docker build --build-arg=FRAPPE_BRANCH=version-13 -t custom-erpnext-nginx:v13 nginx
# build worker镜像
docker build --build-arg=FRAPPE_BRANCH=version-13 -t custom-erpnext-worker:v13 worker
```

这里如果 build 失败，大概率是网络问题，多 build 两次

如果需要其他版本，请参考源库方法：

nginx 镜像：

```
# For edge
docker build -t custom-erpnext-nginx:latest nginx

# For version-12
docker build --build-arg=FRAPPE_BRANCH=version-12 -t custom-erpnext-nginx:v12 nginx

# For version-13
docker build --build-arg=FRAPPE_BRANCH=version-13 -t custom-erpnext-nginx:v13 nginx
```

worker 镜像：

```
# For edge
docker build -t custom-erpnext-worker:latest worker

# For version-12
docker build --build-arg=FRAPPE_BRANCH=version-12 -t custom-erpnext-worker:v12 worker

# For version-13
docker build --build-arg=FRAPPE_BRANCH=version-13 -t custom-erpnext-worker:v13 worker
```



### 6. 复制并修改参数文件

`cp env-example .env`

参数可以参考官方 single-bench 的描述进行修改，地址：https://github.com/frappe/frappe_docker/blob/main/docs/single-bench.md。

需要注意的是，版本需要选择刚刚生成镜像的版本，即 v13 。

- `ERPNEXT_VERSION=v13` # 这里的版本修改为刚才镜像的版本
- `FRAPPE_VERSION=v13` # 这里的版本修改为刚才镜像的版本
- `INSTALL_APPS=erpnext,erpnext_chinese` # 这里添加上 erpnext_chinese
- `SITE_NAME= abc.com`    # 这里使用 abc.com 作为演示域名
- `SITES=abc.com`



### 7. 启动容器。

```bash
# 启动容器，<项目名>修改为自己的。
docker-compose --project-name <项目名> up -d
```

等待 <项目名>_site-creator_1 容器自动停止就通过域名访问（域名需要正确解析，或者添加host）了。



## 其他

site_creator_1 日志可以看到 erpnext_chinese 已经安装成功。

![image1](https://images.gitee.com/uploads/images/2021/1108/143929_de372e38_8164555.png)

![image2](https://images.gitee.com/uploads/images/2021/1108/144006_f39349e8_8164555.png)

常见问题
Debian 10构建打包步骤出现以下权限错误，ubuntu 20没问题

![输入图片说明](https://images.gitee.com/uploads/images/2021/1120/112702_d38bb3d3_7624863.png "屏幕截图.png")
