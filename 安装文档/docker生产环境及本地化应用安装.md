 **前提条件** 
1. linux服务器，且安装了docker,docker-compose 和 git
2. 当前用户如fisher有sudo权限, 
- 添加用户到sudoer group,执行命令 `sudo usermod -aG sudo fisher`
- 允许用户执行sudo时不用每次输密码 执行命令 `sudo visudo` 添加这一行 `fisher ALL=(ALL) NOPASSWD: ALL` 到文件中，ctrl+o 回车，再ctrl+x返回 

 **安装步骤** 
1. 下载官方docker安装项目文件到本地安装目录
`git clone https://github.com/frappe/frappe_docker.git`

2. 切换到安装目录
`cd frappe_docker`

3. 复制生成环境参数文件.env(docker-compose启动配置文件YML默认使用.env参数文件），以下命令根据自己的情况二选一
- `cp env-local .env`      #内网无外网可访问的公开域名，不需要https访问
- `cp env-production .env`      #有外网可访问的公开域名，配置letsencrypt 自动发SSL证书，仅限https方式访问

4. 修改配置参数，可使用nano命令（或各种远程连接方式如vs code,winscp等，用本地文本编辑器修改.env文件的参数）
`nano .env`   在编辑窗口中修改以下关键参数，ctrl + O 回车保存，ctrl + x 返回命令界面, 更多其它参数列表见官网说明，重点是traefik反向代理设置

- `LETSENCRYPT_EMAIL=email@example.com`  #收letsencrypt证书到期的通知用
- `ERPNEXT_VERSION=edge`                 #edge表示开发版，还可写version-12,或version-13-beta，详见github.com/frappe/frappe官网branch清单
- `FRAPPE_VERSION=edge`
- `SITE_NAME= abc.com`    #如果是内网，可以是mysite.local格式的自定义域名，如果是外部公网，填DNS解析纪录中登记过的域名，如果子域名有登记可填子域名，如demo.abc.com
- `SITES=`abc.com``

5. 启动系统(yu是项目/发布名，下同）
`docker-compose --project-name yu up -d`

6. 安装中文本地化应用

- 6.1 进入erpnext-schedule容器命令窗口 `docker exec -it yu_erpnext-schedule_1 bash`

- 6.2 切换到frappe-bench目录 `cd ..`

- 6.3 安装自定义应用为python包 `install_app ebclocal https://gitee.com/yuzelin/ebclocal.git`
 
- 6.4 添加应用名到apps.txt，后面install-app要用 `echo 'ebclocal' >> sites/apps.txt`
 
- 6.5 安装应用到站点 `bench --site abc.com install-app ebclocal`
 
- 6.6 退出容器 `exit`
 
- 6.7 参照build/erpnext-nginx dockerfile中erpnext-nginx镜像安装方式，启动一个基础容器，从宿主机复制安装脚本到容器，进入容器，运行安装脚本下载并生成自定义应用的
html资源文件和js压缩文件
```
sudo docker run -d --name fish bitnami/node:12-prod /bin/sh -c "while true; do echo hello world; sleep 1;done"
sudo docker cp build/erpnext-nginx/install_app.sh fish:/install_app	
sudo docker exec -it fish bash
/install_app ebclocal https://gitee.com/yuzelin/ebclocal.git
exit

```
 - 6.8 将容器中的文件通过宿主机中转复制到erpnext-nginx容器的var/www/html目录下，因为nginx容器的静态资源文件是指向本容器的
```
sudo mkdir js
sudo docker cp fis:/home/frappe/frappe-bench/sites/assets/js/. js
sudo docker cp js/. yu_erpnext-nginx_1:/var/www/html/assets/js
sudo docker cp fis:/home/frappe/frappe-bench/sites/assets/ebclocal .
sudo docker cp ebclocal yu_erpnext-nginx_1:/var/www/html/assets
```
7. 重启erpnext系统
`docker-compose --project-name yu restart`
![输入图片说明](https://images.gitee.com/uploads/images/2020/1109/160435_f60b68d1_7624863.png "屏幕截图.png")
docker-compose运行状态

8. 网页登录安装时设定的域名（如果是内网local方式安装，请在本机c:\windows\systems32\drivers\etc\hosts文件中增加自定义域名和IP的对照关系，或请网管将此对照关系添加到内部DNS记录中）
![输入图片说明](https://images.gitee.com/uploads/images/2020/1109/160502_4882e777_7624863.png "屏幕截图.png")
登录后的网页
